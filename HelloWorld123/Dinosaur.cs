﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld123
{

    // class describes features of dinosaur
    class Dinosaur
    {
      public string Dname { get; set; }
        public int Dheight { get; set; }
        public int Dweight { get; set; }
        public string Dterrain { get; set; }
        public int Did { get; set; }
        //TODO

        //default constructor
        public Dinosaur()
        {
            Dname = "abc";
            Dheight = 30;
            Dweight = 2000;
            Dterrain = "land";
            Did = 123;
        }

        //custom constructor with parameters

        public Dinosaur(string Dname1,int Dheight1,int Dweight1,string Dterrain1,int Did1)
        {
            Dname = Dname1;
          Dheight = Dheight1;
            Dweight = Dweight1;
            Dterrain = Dterrain1;
            Did = Did1;
     

        }
        //function do take input for Dino
       public Dinosaur Input()
        {
            string tempDinoName = "Triceratops";
            int tempDinoHeight = 10;
            int tempDinoWeight = 12;
            string tempDinoTerrain = "North America";
            int tempDinoid = 234;

            Console.WriteLine("enter dino name");
            tempDinoName=Console.ReadLine();

            Console.WriteLine("enter dino height");
            var tempNumber = Console.ReadLine();
            tempDinoHeight = Convert.ToInt32(tempNumber);

            Console.WriteLine("enter dino weight");
            var tempNumber1 = Console.ReadLine();
            tempDinoHeight = Convert.ToInt32(tempNumber1);

            Console.WriteLine("enter dino terrain");
            tempDinoName = Console.ReadLine();

            Console.WriteLine("enter dino id");
            var tempNumber2 = Console.ReadLine();
            tempDinoid = Convert.ToInt32(tempNumber2);

            var toReturnDino = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoid);

            return toReturnDino;

        }

        //function to display Dino
         public void display()
        {
            Console.WriteLine("Dino name" + Dname);
            Console.WriteLine("Dino height" + Dheight);
            Console.WriteLine("Dino weight" + Dweight);
            Console.WriteLine("Dino terrain" + Dterrain);
            Console.WriteLine("Dino id" + Did);
        }
        //function to print all Dino Names
    }
}
